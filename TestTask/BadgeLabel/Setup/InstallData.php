<?php
namespace TestTask\BadgeLabel\Setup;

use Magento\Eav\Setup\EavSetup;

use Magento\Eav\Setup\EavSetupFactory;

use Magento\Framework\Setup\InstallDataInterface;

use Magento\Framework\Setup\ModuleContextInterface;

use Magento\Framework\Setup\ModuleDataSetupInterface;



class InstallData implements InstallDataInterface

{

    private $eavSetupFactory;



    public function __construct(EavSetupFactory $eavSetupFactory)

    {

        $this->eavSetupFactory = $eavSetupFactory;

    }



    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)

    {

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->addAttribute(

            \Magento\Catalog\Model\Product::ENTITY,

            'custom_labels',

            [

                'group' => 'General',

                'attribute_set_id' => 'Bag',

               'type' => 'varchar',

               'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',

               'frontend' => '',

               'label' => 'Custom Labels',

               'input' => 'multiselect',

               'class' => '',

               'source' => '',

               'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,

               'visible' => true,

               'required' => false,

               'user_defined' => true,

               'default' => 0,

               'searchable' => true,

               'filterable' => false,

               'comparable' => true,

               'visible_on_front' => true,

                'used_in_product_listing' => true,

               'sort_order' => 101,

                'option' => ['value' =>
                    [
                        'sale'=>[
                            //option_1 will be static it will add below labels in option_1 for **new**
                            0=>'Sale'    //
                        ],
                        'best_seller'=>[
                            0=>'Best Seller'
                        ],
                        'free_shipping'=>[
                            0=>'Free Shipping'
                        ],
                        'no_options'=>[
                            0=>'No options'
                        ]
                    ],
                    'order'=>//Here We can Set Sort Order For Each Value.
                        [
                            'sale'=>1,
                            'best_seller'=>2,
                            'free_shipping'=>3,
                            'no_options'=>4
                        ]
                ],

            ]

       );

   }

}