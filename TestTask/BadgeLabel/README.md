# Magento 2 Badge Label

This extension allows you to display Badge Labels overlayed on top of the product image. It can shows different things , like: promotions, sales, discounts and so on.  Currently It will display a label if the product is on sale, set as 'free shipping' promotion  and 'best seller' label. Labels will stack on top of each other if multiple labels are valid to show for that product.
Also badge labels can change dynamically when you click on the color attribute of configurable product. You can see them on the  search and product listing.

![Example Image](example.png)

## Getting setup

In order to install this extension you need: 
1. Put folder with content into app/code/ 
2. Enter two cli commands:
* php bin/magento module:enable TestTask_BadgeLabel
* php bin/magento setup:upgrade
## How to enable labels

You need go to admin panel->catalog->products-> then find needed product, click on it and find 'Custom Labels' attribute. After that click on needed label and save product.

![Example Image](example2.png)
# Support

> - NOTE: This was designed for the LUMA theme and works fine with Magento 2.3.

